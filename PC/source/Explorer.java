package com.hkv.explorer;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.MediaTracker;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
//import java.net.UnknownHostException;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.videoio.VideoCapture;

public class Explorer {
	private static final int reconnectInterval = 10000;
	
	private static final String camIP = "192.168.43.1";
	private static final int camPort = 8080;
	private static final String carIP = "192.168.43.2";
	//private static final String carIP = "192.168.1.100";
	private static final int carPort = 12312;
	
	private static Socket socketForCar;
	private static BufferedWriter outToCar;
	private static JFrame frame;
	private static JPanel panel;
	private static JLabel label;
	
	public static void main(String[] args) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

		initViews();
		Thread streamingThread = new StreamingThread();
		streamingThread.start();	
		connectToCar();
		
        panel.addKeyListener(new KeyListener() {
        	
			@Override
			public void keyTyped(KeyEvent e) {
				
			}
			
			@Override
			public void keyReleased(KeyEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyPressed(KeyEvent e) {
				
				String controlMessage = "";
				int keyCode = e.getKeyCode();
				boolean valid = true;

				switch (keyCode) {
				case KeyEvent.VK_W:
					controlMessage = "DDI 0\r\n";
					break;
				case KeyEvent.VK_X:
					controlMessage = "DDI 1\r\n";
					break;
				case KeyEvent.VK_D:
					controlMessage = "DDI 2\r\n";
					break;
				case KeyEvent.VK_A:
					controlMessage = "DDI 3\r\n";
					break;
				case KeyEvent.VK_S:
					controlMessage = "DDI 4\r\n";
					break;
				}
				
				if (valid) {
					System.out.println(controlMessage);
					sendMessage(controlMessage);
				}
			}
		});
	}

	private static void sendMessage(String message) {
		try {
			outToCar.write(message);
			outToCar.flush();
		} catch (IOException e1) {
			connectToCar();
		}
	}
	private static void initViews() {
		frame = new JFrame();
        panel = new JPanel(new BorderLayout());
        frame.getContentPane().add(panel);
        panel.setFocusable(true);
        panel.requestFocusInWindow();
        label = new JLabel();
        panel.add(label, BorderLayout.CENTER);		 
        frame.setSize(new Dimension(640, 480));
        frame.setVisible(true);
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setResizable(false);
	}

	private static void connectToCar() {		
		boolean success = false;
		
		while (!success) {
			System.out.println("Connecting to Car");
			
			try {
				if (socketForCar != null) {
					socketForCar.close();
					socketForCar = null;
				}
				socketForCar = new Socket(carIP, carPort);
				outToCar = new BufferedWriter(new OutputStreamWriter(socketForCar.getOutputStream()));
				success = true;	
			} 
			catch (IOException e2) {
				System.out.println("Connection to Car is lost, reconnect in 10 second");		
				try {
					Thread.sleep(reconnectInterval);
				} catch (InterruptedException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}							
			}
		}
		
		System.out.println("Connected to Car");
	}
	
	private final static class StreamingThread extends Thread {
		private static final VideoCapture capture = new VideoCapture();
		static ImageIcon noSignalImage = new ImageIcon("NO_SIGNAL.jpg"); 

		public void run() {	

			connectToCamera();
			
		    Mat frame = new Mat();
		    while(true)
		    {
		    	if (capture.read(frame)) {
			        label.setIcon(new ImageIcon(toBufferedImage(frame)));
			        panel.revalidate();
			        panel.repaint();
		    	}
		    	else {
		    		connectToCamera();
		    	}
		    }
		}
		
		private static void connectToCamera() {
			System.out.println("Connecting to Camera");

			boolean success = false;
			while(!success) {
				//capture.open("http://88.53.197.250/axis-cgi/mjpg/video.cgi?resolution=320x240");
				//http://192.168.43.1:8080/video
				capture.open("http://" + camIP + ":" + Integer.toString(camPort) + "/video");
				if (capture.isOpened()) {
					success = true;
					System.out.println("Connected to Camera");
				}
				else {
					System.out.println("Connection to Camera is lost, reconnect in 10 second");
					label.setIcon(noSignalImage);
			        panel.revalidate();
			        panel.repaint();
					try {
						Thread.sleep(reconnectInterval);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
	
	private static Image toBufferedImage(Mat m){
	      int type = BufferedImage.TYPE_BYTE_GRAY;
	      if ( m.channels() > 1 ) {
	          type = BufferedImage.TYPE_3BYTE_BGR;
	      }
	      int bufferSize = m.channels()*m.cols()*m.rows();
	      byte [] b = new byte[bufferSize];
	      m.get(0,0,b); // get all the pixels
	      BufferedImage image = new BufferedImage(m.cols(),m.rows(), type);
	      final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
	      System.arraycopy(b, 0, targetPixels, 0, b.length);  
	      return image;

	  }
}