#include <SerialCommand.h>

#include <AFMotor.h>

#include <AFMotor.h>


byte led2[] = {A13,A12,A11};

bool DEBUG = true;
byte timeOut = 30;
byte redColor[] = {255,0,0};
byte greenColor[] = {0,255,0};
byte blueColor[] = {0,0,255};
byte offColor[] = {0,0,0};
byte colorIndex = 0;
byte currentDirection = 4;

const byte DIRECTION_FORWARD = 0;
const byte DIRECTION_BACKWARD = 1;
const byte DIRECTION_TURNLEFT = 3;
const byte DIRECTION_TURNRIGHT = 2;
const byte DIRECTION_NONE = 4;
AF_DCMotor motor1(1, MOTOR12_64KHZ);
AF_DCMotor motor2(2, MOTOR12_64KHZ);
AF_DCMotor motor3(3, MOTOR12_64KHZ);
AF_DCMotor motor4(4, MOTOR12_64KHZ);

SerialCommand sCmd;

void setup() {
  Serial.begin(9600);
  Serial3.begin(115200);
  //Serial3.setTimeout(timeOut);
  // pinMode led RGB
  pinMode(A11, OUTPUT);
  pinMode(A12, OUTPUT);
  pinMode(A13, OUTPUT);
  
  setupEsp8266();
  setupMotor();
  addCommand();
}

void loop() {
  sCmd.readSerial();
  
  
}
void motorForward(){
  setMotorSpeed(255);
  motor1.run(FORWARD);
//  motor2.run(FORWARD);
//  motor3.run(FORWARD);
//  motor4.run(FORWARD);
  currentDirection = DIRECTION_FORWARD;
}
void motorBackward(){
  setMotorSpeed(255);
  motor1.run(BACKWARD);
//  motor2.run(BACKWARD);
//  motor3.run(BACKWARD);
//  motor4.run(BACKWARD);
  currentDirection = DIRECTION_BACKWARD;
}
void motorTurnLeft(){
  if(currentDirection == DIRECTION_FORWARD){
    motor1.setSpeed(64);
    motor2.setSpeed(64);
    motor3.setSpeed(255);
    motor4.setSpeed(255);
    
    motor1.run(FORWARD);
    motor2.run(FORWARD);
  }
  else{
    setMotorSpeed(154);
    motor1.run(BACKWARD);
    motor2.run(BACKWARD);
  }
  motor3.run(FORWARD);
  motor4.run(FORWARD);
  currentDirection = DIRECTION_TURNLEFT;
}
void motorTurnRight(){
  if(currentDirection == DIRECTION_FORWARD){
    motor1.setSpeed(255);
    motor2.setSpeed(255);
    motor3.setSpeed(64);
    motor4.setSpeed(64);
    
    motor3.run(FORWARD);
    motor4.run(FORWARD);
  }else{
    setMotorSpeed(164);
    motor3.run(BACKWARD);
    motor4.run(BACKWARD);
  }
  motor1.run(FORWARD);
  motor2.run(FORWARD);
  currentDirection = DIRECTION_TURNRIGHT;
}
void motorStop(){
  motor1.run(RELEASE);
  motor2.run(RELEASE);
  motor3.run(RELEASE);
  motor4.run(RELEASE);
  currentDirection = DIRECTION_NONE;
}
void setMotorSpeed(byte sp){
  motor1.setSpeed(sp);
  motor2.setSpeed(sp);
  motor3.setSpeed(sp);
  motor4.setSpeed(sp);
}
void setupEsp8266(){
  sendData("AT+RESTORE",1000,DEBUG); // Khôi phục cài đặt
  sendData("AT+RST",1000,DEBUG); // RESET ESP8266
  sendData("AT+CWMODE=2",1000,DEBUG); // Chọn chế độ trạm phát wifi
 // sendData("AT+CWSAP=\"HKV\",\"khongcomang\",1,3",1000,DEBUG); //Cài đặt cho mạng wifi
  sendData("AT+CIPMUX=1",1000,DEBUG); // Bật chế độ đa kết nối
 // sendData("AT+CIPSTA=\"192.168.4.193\"",1000,DEBUG); //Set ip tĩnh
  sendData("AT+CIFSR",1000,DEBUG); // Hiển thị ip
  sendData("AT+CIPSERVER=1,80",1000,DEBUG); // Khởi động Server ở port 80
}
void setupMotor(){
  setMotorSpeed(255);
}

String sendData(String command, const int timeout, boolean debug)
{
    String response = "";
    Serial3.println(command);
    long int time = millis();
    
    while( (time+timeout) > millis()){
      while(Serial3.available()){
        char c = Serial3.read();
        response+=c;
      }  
    }
    
    if(debug){
      Serial.print(response);
    }
    return response;
}
void sendMessenge(String messenge){
  String cipSend = "AT+CIPSEND=0,";
  cipSend += messenge.length();
  sendData(cipSend,timeOut,false);
  sendData(messenge, timeOut, false);
}

void setColorLed(byte led[],byte color[]){
  analogWrite(led[0],color[0]);
  analogWrite(led[1],color[1]);
  analogWrite(led[2],color[2]);
}
void changeColor(){
  switch(colorIndex){
    case 0: 
      setColorLed(led2, greenColor);
      colorIndex=1;
    break;
    case 1: 
      setColorLed(led2, redColor);
      colorIndex=0;
    break;
  }
}
void directionUpdate(){
  char *arg;
  arg = sCmd.next();
  if(arg != NULL){
    switch(atoi(arg)){
      case 0: motorForward();
      break;
      case 1: motorBackward();
      break;
      case 2: motorTurnRight();
      break;
      case 3: motorTurnLeft();
      break;
      case 4: motorStop();
      break;
    }
  }
}
void buttonListenner(){
  char *arg;
  arg = sCmd.next();
  if(strcmp(arg, "T")){
    setColorLed(led2, offColor);
  }else if(strcmp(arg, "B")){
    changeColor();
  }else if(strcmp(arg, "C")){
    //Chưa làm gì
  }else if(strcmp(arg, "R")){
    //Chưa làm gì
  }else if(strcmp(arg, "L")){
    //Chưa làm gì
  }
  
}
void addCommand(){
  sCmd.addCommand("DDI", directionUpdate);
  sCmd.addCommand("DBT", buttonListenner);
  sCmd.addDefaultHandler(unrecognized);
}
void unrecognized(const char *command){
  Serial.println("What?");
}
