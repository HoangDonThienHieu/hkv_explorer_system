package code_dk;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;


public class Client{
	private static Socket client;
	private static BufferedWriter outToServer;
	private static JFrame frame;
	private static JPanel panel;
	
	public static void main(String[] args) {
		initViews();
		
		System.out.println("Connecting");
		while (true) {
			try {
				client = new Socket("192.168.1.103", 1336);
				outToServer = new BufferedWriter(new OutputStreamWriter(client.getOutputStream()));
				System.out.println("Connected!");	
				while (true) {
			        panel.addKeyListener(new KeyListener() {
						
						@Override
						public void keyTyped(KeyEvent e) {
							
						}
						
						@Override
						public void keyReleased(KeyEvent e) {
							// TODO Auto-generated method stub
							
						}
						
						@Override
						public void keyPressed(KeyEvent e) {
							int keyCode = e.getKeyCode();
							String m = "";
							switch (keyCode) {
							case KeyEvent.VK_UP:
								m = "DDI 0\r\n";
								break;
							case KeyEvent.VK_DOWN:
								m = "DDI 1\r\n";
								break;
							case KeyEvent.VK_RIGHT:
								m = "DDI 2\r\n";
								break;
							case KeyEvent.VK_LEFT:
								m = "DDI 3\r\n";
								break;
							default:
								m = "DDI 4\r\n";
								break;
							}
							try {
								outToServer.write(m);
								outToServer.flush();
								System.out.println(m);
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					});
				}
			
			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				//e.printStackTrace();
				System.out.println("Failed to connect, reconnecting");
			}
		}
	}

	private static void initViews() {
		frame = new JFrame();
        panel = new JPanel(new BorderLayout());
        frame.getContentPane().add(panel);
        panel.setFocusable(true);
        panel.requestFocusInWindow();
        ImageIcon image = new ImageIcon("D:\\arduinovn.png");
        JLabel label = new JLabel("", image, JLabel.CENTER);
        panel.add(label, BorderLayout.CENTER );
        frame.setSize(new Dimension(480, 480));
        frame.setVisible(true);
	}

}
