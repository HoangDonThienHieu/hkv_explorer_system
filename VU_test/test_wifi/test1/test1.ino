#include <AFMotor.h>

String response;
int num;

byte currentDirection = 4;

const byte DIRECTION_FORWARD = 0;
const byte DIRECTION_BACKWARD = 1;
const byte DIRECTION_TURNLEFT = 3;
const byte DIRECTION_TURNRIGHT = 2;
const byte DIRECTION_NONE = 4;

AF_DCMotor motor1(1, MOTOR12_64KHZ);

void setup() {
  delay(1000);

  Serial.begin(9600);
  Serial3.begin(115200);

  Serial.println("Configure ESP");
  setupEsp8266();
  setupMotor();
}

void loop() {
  if (Serial3.available())
  {
    if (Serial3.find("+IPD,"))
    {
      delay(1000);
      response = "";
      if (Serial3.find("DDI "))
      {
        num = Serial3.read() - 48;
        switch (num)
        {
          case 0:
            Serial.println("Forward");
            motorForward();
            while (Serial3.available())
              Serial3.read();
            break;
          case 1:
            Serial.println("Backward");
            motorBackward();
            while (Serial3.available())
              Serial3.read();
            break;
          case 2:
            Serial.println("2");
            while (Serial3.available())
              Serial3.read();
            break;
          case 3:
            Serial.println("3");
            while (Serial3.available())
              Serial3.read();
            break;
          case 4:
            Serial.println("Stop");
            motorStop();
            while (Serial3.available())
              Serial3.read();
            break;
          default:
            while (Serial3.available())
              Serial3.read();
            break;
        }
      }
      else
      {
        Serial.println("Invalid command");
      }
    }
  }

}

void setupEsp8266() {
  deliverMessage("AT+RESTORE\r\n", 1000);
  delay(5000);
  deliverMessage("AT+RST\r\n", 2000);
  delay(5000);
  deliverMessage("AT+CWMODE=3\r\n", 3000);
  //deliverMessage("AT+CWSAP=\"VU\",\"12345678\",1,4\r\n", 3000);
  deliverMessage("AT+CWJAP=\"tinyboy186\",\"hamster2001\"\r\n",3000);
  delay(5000);
  deliverMessage("AT+CIFSR\r\n", 1000);
  deliverMessage("AT+CIPMUX=1\r\n", 1000);
  deliverMessage("AT+CIPSERVER=1,1336\r\n", 1000);
}

void deliverMessage(const String& msg, int dt) {
  Serial3.print(msg);

  long int time = millis();

  response = "";
  while ((time + dt) > millis()) {
    while (Serial3.available()) {
      char c = Serial3.read();
      response += c;
    }
  }
  Serial.print(response);
}

void motorForward(){
  setMotorSpeed(255);
  motor1.run(FORWARD);
  currentDirection = DIRECTION_FORWARD;
}

void motorBackward(){
  setMotorSpeed(255);
  motor1.run(BACKWARD);
  currentDirection = DIRECTION_BACKWARD;
}

void motorStop(){
  motor1.run(RELEASE);
  currentDirection = DIRECTION_NONE;
}

void setMotorSpeed(byte sp){
  motor1.setSpeed(sp);
}

void setupMotor(){
  setMotorSpeed(255);
}

